package com.example.admin.lab1;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.jar.Attributes;
import java.util.prefs.Preferences;


public class MainActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SQLiteDatabase db = openOrCreateDatabase(Preferences.DB_NAME,
                Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS People (Id INTEGER PRIMARY KEY, Attributes.Name VARCHAR(32))");
        Cursor dbResult = db.rawQuery("SELECT * FROM People", null);
        // do sometning with cursors
        dbResult.close();
        db.close();

        String[] str = new String[] { "one","two","three","four","five","six"};
        ArrayAdapter<String> arrAdpt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, str);
        ListView lv = (ListView)findViewById(R.id.listView1);
        setContentView(R.layout.activity_main);
        lv.setAdapter(arrAdpt);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
